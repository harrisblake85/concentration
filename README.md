# Concentration Card Game
## Author: Blake Harris
#### Live Link to the project (altered for github pages):
#### http://concentration.paigesprojects.com/

![](https://snag.gy/Klg64j.jpg)
### Game Description
-	When the user clicks the ‘New Game’ button, shuffle the deck of cards, then layout the cards, face down, in 4-rows with 13 cards in each row.
-	The user clicks on two cards. As the user clicks on a card, the card is flipped over so users can see the # and suit of the card.
-	If the numbers on the two cards selected by the user are the same, then those two cards are removed from the board. If the numbers on the cards do not match, then the cards are flipped back over and remain on the board.
-	The game ends when the user has matched all cards.


### How to test on your computer

- cd into ./concentration
- npm i
- node server.js
- http://localhost:3001/
